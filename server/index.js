const express = require("express");
const cookieSession = require("cookie-session");
const passport = require("passport");
const keys = require("./config/keys");
require("./models/User");
require("./services/passport");

const app = express();
const mongoose = require("mongoose");

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieKey]
  })
);
app.use(passport.initialize());
app.use(passport.session());

mongoose.connect(keys.MongoDB.connectionString);

require("./routes/authRoutes")(app);

//Dynamic port Binding
const PORT = process.env.PORT || 5000;
app.listen(PORT);
